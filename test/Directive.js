import test from 'ava';
const Directive = require('../src/Directive.js');

test('can be made from string', async t => {
  const value = 'default-src \'self\' https: example.com';
  const testDirective = Directive.fromString(value);
  const expectedArray = [
    '\'self\'',
    'https:',
    'example.com'
  ];
  t.deepEqual(testDirective.rules, expectedArray);
  t.deepEqual(testDirective.name, 'default-src');
  t.notDeepEqual(testDirective.name, 'script-src');
});

test('can check that it contains a value', async t => {
  const value = 'default-src example.com';
  const testDirective = Directive.fromString(value);
  t.true(testDirective.contains('example.com'));
  t.false(testDirective.contains('\'self\''));
});
