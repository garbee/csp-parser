'use strict';
/**
 * @constructor
 *
 * @property {string} name - The name of the directive.
 * @property {!Array<string>} rules - The rules for the directive.
 *
 * @param {string} name The name of the directive.
 * @param {!Array<string>} rules The rules for the directive.
 */
module.exports = class Directive {
  constructor(name, rules) {
    this._name = name;
    this._rules = rules;
  }

  /**
   * @return {string} Directive's name
   */
  get name() {
    return this._name;
  }

  /**
   * @return {!Array<string>} Directive's rules.
   */
  get rules() {
    return this._rules;
  }

  /**
   * @param {string} value The value to look for in the directive.
   * @return {bool} Whether the value exists in the directive.
   */
  contains(value) {
    let found = false;
    this._rules.forEach(item => {
      if (item.trim() === value.trim()) {
        found = true;
      }
    });
    return found;
  }

  /**
   * @param {string} value The directive value to parse.
   * @return {Directive} CSP Directive
   */
  static fromString(value) {
    const items = value.trim().split(' ');
    const name = items.shift().trim();
    return new Directive(name, items);
  }
};
