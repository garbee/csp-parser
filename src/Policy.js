const Directive = require('./Directive.js');
/**
 * @property {Array<Directive>} directives The directives of the policy.
 *
 * @param {Array<Directive>} directives The directives of the policy.
 */
module.exports = class Policy {
  constructor(directives) {
    if (directives.constructor.name !== 'Array') {
      throw new Error('An array of directives must be provided');
    }
    directives.forEach(directive => {
      if (directive.constructor.name !== 'Directive') {
        throw new Error('Only directive objects may be given to a policy.');
      }
    });
    this._directives = directives;
    
    this._knownDirectives = [
      'base-uri',
      'child-src',
      'connect-src',
      'default-src',
      'font-src',
      'form-action',
      'frame-ancestors',
      'frame-src',
      'img-src',
      'manifest-src',
      'media-src',
      'object-src',
      'plugin-types',
      'referrer',
      'reflected-xss',
      'report-uri',
      'sandbox',
      'script-src',
      'style-src',
      'upgrade-insecure-requests'
    ];
    this._deprecatedDirectives = [
      {
        name: 'frame-src',
        alternative: 'child-src'
      }
    ];
  }

  /**
   * @return {Array<Directive>} The directives of the policy.
   */
  get directives() {
    return this._directives;
  }

  /**
   * @param {string} name The directive name to look for.
   * @return {bool} Whether the directive name is included in the policy.
   */
  containsDirective(name) {
    return Boolean(this.getDirective(name));
  }

  /**
   * @param {string} name The name of the directive to retrieve.
   * @return {?Directive} The directive if found, null otherwise.
   */
  getDirective(name) {
    let directive = null;
    this._directives.forEach(item => {
      if (item.name === name) {
        directive = item;
      }
    });
    return directive;
  }
  
  /**
   * @return {Array} The array of deprected directive objects.
   */
  get deprecatedDirectives() {
    return this._deprecatedDirectives;
  }
  
  /**
   * @return {Array<?Directive>} The directives if any found to be deprecated.
   */
  get deprecatedDirectivesInPolicy() {
    const policies = [];
    this._deprecatedDirectives.forEach(directive => {
      this._directives.forEach(item => {
        if (directive.name === item.name) {
          policies.push(item);
        }
      });
    });
    return policies;
  }
  
  /**
   * @return {bool} Indicates whether the policy contains a deprecated directive.
   */
  get usesDeprecatedDirective() {
    return Boolean(this.deprecatedDirectivesInPolicy.length);
  }

  /**
   * @param {string} value The string to construct a CSP Policy from.
   * @return {Policy} The policy object.
   */
  static fromString(value) {
    let items = value.trim().split(';');
    let directives = [];
    items.forEach(item => {
      if (item.trim() !== '') {
        directives.push(Directive.fromString(item));
      }
    });
    return new Policy(directives);
  }
};
