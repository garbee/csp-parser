import test from 'ava';
const Policy = require('../src/Policy.js');
const Directive = require('../src/Directive.js');

test('can be made from string', async t => {
  const value = 'default-src example.com; script-src \'self\';';
  const testPolicy = Policy.fromString(value);
  t.deepEqual(testPolicy.directives.length, 2);
});

test('contains checks for a given directive name', async t => {
  const testPolicy = Policy.fromString('default-src example.com');
  t.true(testPolicy.containsDirective('default-src'));
  t.false(testPolicy.containsDirective('script-src'));
});

test('can retrive a directive by name', async t => {
  const testPolicy = Policy.fromString(
    'default-src example.com; script-src \'self\';');
  const expectedDirective = Directive.fromString('default-src example.com');
  t.deepEqual(testPolicy.getDirective('default-src'), expectedDirective);
});

test('is made properly with excessive separators', async t => {
  const testPolicy = Policy.fromString(
    'default-src example.com; script-src example.com;; ;   ;frame-src example.com'
    );
  t.deepEqual(testPolicy.directives.length, 3);
  t.true(testPolicy.containsDirective('default-src'));
  t.true(testPolicy.containsDirective('script-src'));
  t.true(testPolicy.containsDirective('frame-src'));
  t.false(testPolicy.containsDirective(''));
});

test('detects if a policy uses a deprecated directive', t => {
  const testPolicy = Policy.fromString(
'default-src example.com; script-src \'unsafe-inline\'; frame-src \'self\''
  );
  t.true(testPolicy.usesDeprecatedDirective);
});

test('returns the deprecated directives', t => {
  const expectedDirectives = [
    Directive.fromString('frame-src \'self\'')
  ];
  const testPolicy = Policy.fromString(
'default-src example.com; script-src \'unsafe-inline\'; frame-src \'self\''
  );
  t.deepEqual(
    testPolicy.deprecatedDirectivesInPolicy,
    expectedDirectives
  );
});
